# 1) Administración de estados 

## Preparar backend.

> **Requerimiento**
> - Bucket de Cloud Storage *[backend-znt-sqmworkshop-xx]*
> - Cuenta de Servicio con permisos de "Cloud storage editor" *[credentials.json]*

## Crear backend

Es posible inicializar un backend por comandos o bien mediante un manifiesto tf
En la raiz creamos el archivo ```backend.tf```
```
# backend.tf
terraform {
  backend "gcs" {
    bucket  = "<BUCKET_NAME>"
    prefix  = "tf-app"
    credentials = "./credentials.json"
  }
}

```
```
# Command line, esta opcion requiere que se declare como minimo un archivo de backend parcial
# backend.tf

terraform {
  backend "gcs" {
  }
}

# command
$ terraform init \
    -backend-config="bucket=<bucket>" \
    -backend-config="prefix=tf-app" \
    -backend-config="credentials=./credentials.json"
```

(Opcional) Finalmente habilitamos el versionado sobre el storage, en la consola de GCP:
```
$ gsutil versioning set on gs://<BUCKET_NAME>

```
Directorio de referencia: ( [**p1**](./example/p1) ) 

# 2) Workspaces y Segmentación de ambientes

Cada configuración de Terraform tiene un backend asociado que define cómo se ejecutan las operaciones y dónde se almacenan los datos persistentes, como el estado de Terraform .

Los datos persistentes almacenados en el backend pertenecen a un **"Workspace"** . Inicialmente, el backend tiene solo un espacio de trabajo, llamado **"default"** y por lo tanto, solo hay un estado de Terraform asociado con esa configuración.

```
$ terraform workspace list

```

```
.
├── bucket
    ├──prefix
        ├── default.tfstate   <-- Workspace
```
## Crear nuevos workspace

Para segmentar ambientes, utilizaremos "Workspace" para separar nuestro backend en multiples configuraciones.

Para ello creamos el workspace "dev" y "prod" utilizando los siguientes comandos

```
$ terraform workspace new <NAME>      # Crear workspace
$ terraform workspace list            # Listar  workspace
$ terraform workspace select <NAME>   # Seleccionar el workspace


# En un nivel logico, la estructura de workspace en el backend queda de la siguiente forma
.
├── bucket
    ├──prefix
        ├── default.tfstate   <-- Workspace
        ├── dev.tfstate       <-- Workspace
        ├── prod.tfstate      <-- Workspace
```

Un uso común para múltiples espacios de trabajo es crear una copia distinta y paralela de un conjunto de infraestructura para probar un conjunto de cambios antes de modificar la infraestructura de producción principal.


# 3) Segmentar configuraciones 

 ## **[INFO]** Crear variables "locales"
Un valor local asigna un nombre a una expresión , por lo que puede usarlo varias veces dentro de un módulo sin repetirlo.

Si está familiarizado con los lenguajes de programación tradicionales, puede ser útil comparar los módulos de Terraform con las definiciones de funciones:
```
variables "<NAME>"{ 
  # Variables de entrada utilizadas como argumentos de función.
}  

output "<NAME>"{ 
  # Son como valores de retorno de funciones.
}  

locals { 
  # Equivalen a variables locales temporales de una función.
  project_name = "some_vale"
}   
```
Para utilizar una variable ```locals``` debe hacer uso del prefijo ```local.<NAME>```
```
module "cdn" {
  source                        = "./modules/terraform-gcp-cdn"
  # ...
  project_name                  = local.project_name

}
```

## Segmentar variables reutilizables - Implementar variables locales

Creamos el archivo ```locals.tf```, y añadimos el siguiente contenido

```
locals {
  env = terraform.workspace == "default" ? "not use default workspace, instead use dev (dev), prod       (prod      )" : terraform.workspace
  
  project_ids = {
    dev              = "znt-sqmworkshop-01"
    prod             = "znt-sqmworkshop-01"
  }
  environments = {
    dev              = "dev"
    prod             = "prod"
  }

  regions = {
    dev              = "us-central1"
    prod             = "us-central1"
  }

  zones = {
    dev              = "us-central1-c"
    prod             = "us-central1-c"
  }
 # lookup recupera el valor de un solo elemento de un mapa, dada su clave. Si la clave dada no existe, se devuelve el valor predeterminado dado.
 # lookup(map, key, default) 
 # DOC: https://www.terraform.io/docs/language/functions/lookup.html

  project_id                          = lookup( local.project_ids, local.env)
  environment                         = lookup( local.environments, local.env)
  region                              = lookup( local.regions, local.env)
  zone                                = lookup( local.zones, local.env)
}
```

Invocamos las variables locales desde nuestros recursos, para ello modificamos el archivo raiz ```main.tf``` y ```provider.tf```
``` 
# main.tf
module "vpc" {
  source          = "./modules/terraform-gcp-vpc"
  network_name    = var.network_name        <-- "${var.network_name}-${local.environment}"
}

module "instance" {
  source          = "./modules/terraform-gcp-compute-engine"

  network_name    = module.vpc.vpc_name
  instance_name   = var.instance_name       <-- "${var.instance_name}-${local.environment}"
  instance_type   = var.instance_type
  instance_image  = var.instance_image
}

```
``` 
# provider.tf

## Terraform config
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.0.0"
    }
  }
}

## Providers
provider "google" {
  credentials = file(var.credentials_file)

  project = var.project   <-- local.project_ids
  region  = var.region    <-- local.region
  zone    = var.zone      <-- local.zone
}

```
## Segmentar parametros de infraestructura - TFVALUES

- Crear multiples tfvalues (terraform-dev.tfvars y terraform-prod.tfvars)
- Aplicar cambios de infraestuctura con terraform utilizando los parametros correspondientes.
```
$ terraform apply -var-file="<FILE>.tfvars"
```

Directorio de referencia: ( [**p2**](./example/p2) ) 

