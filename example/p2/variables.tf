## GLOBAL ENVIRONMENT
variable "project" {}
variable "credentials_file" {}
# variable "region" {}
# variable "zone" {}

## VPC ENVIRONMENT
variable "net_name" {}

## INSTANCE ENVIRONMENT

variable "instance_name" {}
variable "instance_image" {}
variable "instance_type" {}